
package cifrassignificativa;

import java.util.Scanner;


public class Aritmetica {

            
    Scanner entrada = new Scanner(System.in);

    public Aritmetica() {
    }
    
    public void una(){
        System.out.println("Ingresa la cantidad: ");              
        String cantidad = entrada.next();       
        char[] caracteres = cantidad.toCharArray();
        boolean punto=false;
                                
        for(int i=0;i<caracteres.length;i++){       
            if(caracteres[i]=='.'){
                punto=true;                 
            }                 
        }
                
        if(punto!=true){       
            int num=Integer.parseInt(cantidad);
            int n, cifras;
            n = num;            
            cifras= 0;      
                  
            while(n!=0)
            {             
                n = n/10;                         
                cifras++;             
            }
            
            System.out.println("El número "+cantidad+ " tiene " + cifras+ " cifras significativas");
               
        }else{
            int cifras = 0;
            int posicion=0;

            if(caracteres[posicion]>'0'){     
                posicion=0;          
                cifras=-1;
                
            }else{
                while(caracteres[posicion]!='.'){
                    posicion++;                    
                }   
            }
                    
            int pos = posicion;
            while(caracteres[pos]<='0'){
                pos++;    
            }
            
            for(int a=pos;a<caracteres.length;a++){
                cifras++;                        
            }
            System.out.println("El numero "+cantidad+" Tiene "+cifras+" Cifras Significativas ");
        }
    }
    
    public void suma(){
        
        
        boolean hayentero = false;        
        boolean hayentero2 = false;       
        System.out.println("¿Cuantas cantidades deseas Sumar?");
        int n = entrada.nextInt();
        double array[] = new double[n+1];        
        System.out.println("Ingresar cantidades a Sumar: ");        
        double suma = 0;  
        for(int i=1;i<array.length;i++){
            System.out.print(i+".- ");
            array[i]=entrada.nextDouble();
            suma=suma+array[i];
            if(array[i]% 1 == 0){
                hayentero=true;
                if(hayentero==true){
                    hayentero2=true;
                }
            }                        
        }
                
               
        System.out.println("Resultado: "+suma);               
        double decimal[] = new double[n+1];        
        int o = 0;

                
        for(int i=1;i<array.length;i++){     
            double doble = array[i];
            String letra = String.valueOf(doble);
            char[] busca = letra.toCharArray();

            int posicion=0;
            while(busca[posicion]!='.'){
                posicion++;  
            }
                   
            int decimales=0;
            for(int a=posicion+1;a<busca.length;a++){       
                decimales++;                
            }
            decimal[o]=decimales;
            o++;
        }  
                              
        for (int i=0; i<decimal.length-1;i++)
        {
            int min=i;
            for (int j=i+1; j<decimal.length; j++)
            {
                if (decimal[j]<decimal[min])
                {
                    min=j;
                }       
            }                 
            if (i!=min)            
            {           
                double aux= decimal[i];              
                decimal[i] = decimal[min];              
                decimal[min] = aux;              
            }           
        }                

                
        String l = String.valueOf(suma);
        char[] b = l.toCharArray();               
        int i=0;        
        int pos=0;        
        while(b[pos]!='.'){
            pos++;           
        }
             
        double total = pos+decimal[1];
        int canti = 0;
        
        for(int e=0;e<total+1;e++){
            canti++;
        }
             
                
        if(hayentero2==true){
            pos=0;
            while(b[pos]!='.'){
                pos++;
            } 
            char w = b[canti-1];         
            String ti = Character.toString(w);    
            double xc = Double.parseDouble(ti);
                   
            if(xc>=5){                   
                char codigo = b[pos-1];               
                String cadena = Character.toString(codigo);                
                double doble = Double.parseDouble(cadena);                
                doble=doble+1;                
                double totalDoble = doble;
                String totalStrig = String.valueOf(doble);
                String co= totalStrig;
                char caracter = co.charAt(0);
                b[pos-1]=caracter;  
                
            }else{
                if(xc<=4){ 
                    char codigo = b[canti-1]; 
                    String cadena = Character.toString(codigo); 
                    double doble = Double.parseDouble(cadena);  
                    doble=doble;
                    double totalDoble = doble;  
                    String totalStrig = String.valueOf(doble);
                    String co= totalStrig;
                    char caracter = co.charAt(0);
                    b[canti-1]=caracter;
                }  
              
            }                         
            System.out.print("Resultado correcto: ");           
            for(int e=0;e<pos;e++){           
                canti++;               
                System.out.print(b[e]);              
            }          
        }else{
            char m;

            try{
                m = b[canti];                         
            }catch(Exception a){                
                m = b[canti-1];                                         
            }
            
            String c = Character.toString(m);    
            double d = Double.parseDouble(c);

            if(d>=5){
                char codigo = b[canti-1];          
                String cadena = Character.toString(codigo);
                double doble = Double.parseDouble(cadena);
                doble=doble+1;                
                double totalDoble = doble;
                String totalStrig = String.valueOf(doble);
                String co= totalStrig;
                char caracter = co.charAt(0);                
                b[canti-1]=caracter;
                                
            }else{
                       
                if(d<=4){
                    char codigo = b[canti-1]; 
                    String cadena = Character.toString(codigo); 
                    double doble = Double.parseDouble(cadena);  
                    doble=doble;
                    double totalDoble = doble;  
                    String totalStrig = String.valueOf(doble);
                    String co= totalStrig;
                    char caracter = co.charAt(0);
                    b[canti-1]=caracter;
                }    
            }
                    
            System.out.print("Resultado correcto: ");
            for(int e=0;e<total+1;e++){            
                canti++;           
                System.out.print(b[e]);   
            }     
        }
    } 
    
    public void restar(){
        
        
        boolean hayentero = false;        
        boolean hayentero2 = false;       
        System.out.println("¿Cuantas cantidades deseas Restar?");
        int n = entrada.nextInt();
        double array[] = new double[n+1];        
        System.out.println("Ingresar cantidades a Restar: ");        
        double restar = 0;  
        for(int i=1;i<array.length;i++){
            System.out.print(i+".- ");
            array[i]=entrada.nextDouble();
            
            if(i==1){
                restar = array[i];   
            }else{
                restar=restar-array[i];
                
            }

            if(array[i]% 1 == 0){
                hayentero=true;
                if(hayentero==true){
                    hayentero2=true;
                }
            }                        
        }
                
               
        System.out.println("Resultado: "+restar);               
        double decimal[] = new double[n+1];        
        int o = 0;

                
        for(int i=1;i<array.length;i++){     
            double doble = array[i];
            String letra = String.valueOf(doble);
            char[] busca = letra.toCharArray();

            int posicion=0;
            while(busca[posicion]!='.'){
                posicion++;  
            }
                   
            int decimales=0;
            for(int a=posicion+1;a<busca.length;a++){       
                decimales++;                
            }
            decimal[o]=decimales;
            o++;
        }  
                              
        for (int i=0; i<decimal.length-1;i++)
        {
            int min=i;
            for (int j=i+1; j<decimal.length; j++)
            {
                if (decimal[j]<decimal[min])
                {
                    min=j;
                }       
            }                 
            if (i!=min)            
            {           
                double aux= decimal[i];              
                decimal[i] = decimal[min];              
                decimal[min] = aux;              
            }           
        }                

                
        String l = String.valueOf(restar);
        char[] b = l.toCharArray();               
        int i=0;        
        int pos=0;        
        while(b[pos]!='.'){
            pos++;           
        }
             
        double total = pos+decimal[1];
        int canti = 0;
        
        for(int e=0;e<total+1;e++){
            canti++;
        }
             
                
        if(hayentero2==true){
            pos=0;
            while(b[pos]!='.'){
                pos++;
            } 
            char w = b[canti-1];         
            String ti = Character.toString(w);    
            double xc = Double.parseDouble(ti);
                   
            if(xc>=5){                   
                char codigo = b[pos-1];               
                String cadena = Character.toString(codigo);                
                double doble = Double.parseDouble(cadena);                
                doble=doble+1;                
                double totalDoble = doble;
                String totalStrig = String.valueOf(doble);
                String co= totalStrig;
                char caracter = co.charAt(0);
                b[pos-1]=caracter;  
                
            }else{
                if(xc<=4){ 
                    char codigo = b[canti-1]; 
                    String cadena = Character.toString(codigo); 
                    double doble = Double.parseDouble(cadena);  
                    doble=doble;
                    double totalDoble = doble;  
                    String totalStrig = String.valueOf(doble);
                    String co= totalStrig;
                    char caracter = co.charAt(0);
                    b[canti-1]=caracter;
                }  
              
            }                         
            System.out.print("Resultado correcto: ");           
            for(int e=0;e<pos;e++){           
                canti++;               
                System.out.print(b[e]);              
            }          
        }else{
            char m;

            try{
                m = b[canti];                         
            }catch(Exception a){                
                m = b[canti-1];                                         
            }
            
            String c = Character.toString(m);    
            double d = Double.parseDouble(c);

            if(d>=5){
                char codigo = b[canti-1];          
                String cadena = Character.toString(codigo);
                double doble = Double.parseDouble(cadena);
                doble=doble+1;                
                double totalDoble = doble;
                String totalStrig = String.valueOf(doble);
                String co= totalStrig;
                char caracter = co.charAt(0);                
                b[canti-1]=caracter;
                                
            }else{
                       
                if(d<=4){
                    char codigo = b[canti-1]; 
                    String cadena = Character.toString(codigo); 
                    double doble = Double.parseDouble(cadena);  
                    doble=doble;
                    double totalDoble = doble;  
                    String totalStrig = String.valueOf(doble);
                    String co= totalStrig;
                    char caracter = co.charAt(0);
                    b[canti-1]=caracter;
                }    
            }
                    
            System.out.print("Resultado correcto: ");
            for(int e=0;e<total+1;e++){            
                canti++;           
                System.out.print(b[e]);   
            }     
        }
    } 
}
